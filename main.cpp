#include <iostream>
#include <time.h>
#include <stdlib.h>
#include<bits/stdc++.h>
#include<string.h>

#define RANGE 255
using namespace std;

bool isPrime(int n)
{
    // Corner case
    if (n <= 1)  return false;

    // Check from 2 to n-1
    for (int i=2; i<n; i++)
        if (n%i == 0)
            return false;

    return true;
}

void countSort(char arr[])
{
    // The output character array
    // that will have sorted arr
    char output[strlen(arr)];

    // Create a count array to store count of inidividul
    // characters and initialize count array as 0
    int count[RANGE + 1], i;
    memset(count, 0, sizeof(count));

    // Store count of each character
    for(i = 0; arr[i]; ++i)
        ++count[arr[i]];

    // Change count[i] so that count[i] now contains actual
    // position of this character in output array
    for (i = 1; i <= RANGE; ++i)
        count[i] += count[i-1];

    // Build the output character array
    for (i = 0; arr[i]; ++i)
    {
        output[count[arr[i]]-1] = arr[i];
        --count[arr[i]];
    }

    for (i = 0; arr[i]; ++i)
        arr[i] = output[i];
}


void swap(int* a, int* b)
{
    int t = *a;
    *a = *b;
    *b = t;
}
int partition (int arr[], int low, int high)
{
    int pivot = arr[high]; // pivot
    int i = (low - 1); // Index of smaller element

    for (int j = low; j <= high - 1; j++)
    {
        // If current element is smaller than the pivot
        if (arr[j] < pivot)
        {
            i++; // increment index of smaller element
            swap(&arr[i], &arr[j]);
        }
    }
    swap(&arr[i + 1], &arr[high]);
    return (i + 1);
}

void quickSort(int arr[], int low, int high)
{
    if (low < high)
    {
        /* pi is partitioning index, arr[p] is now
        at right place */
        int pi = partition(arr, low, high);

        // Separately sort elements before
        // partition and after partition
        quickSort(arr, low, pi - 1);
        quickSort(arr, pi + 1, high);
    }
}

/* Function to print an array */
void printArray(int arr[], int size)
{
    int i;
    for (i = 0; i < size; i++)
        cout << arr[i] << " ";
    cout << endl;
}

int main() {
    int x = 0;
    int arr[10];
    char ar[] = "s8b5jg483n";
    srand(time(NULL));
    cout<<"IZBERI\n";
    cout<<"1 - Quicksort"<<endl;
    cout<<"2 - Counting sort"<<endl;
    cout<<"3 - Primary Test"<<endl;
    cout<<"Tvoja izbira: ";
    cin>>x;
    switch (x){
        case 1:
            for(int i = 0; i < 10; i++)
                arr[i] = rand()%100;
            printArray(arr, 10);
            cout<<endl<<endl;
            quickSort(arr, 0, 9);
            printArray(arr, 10);
            break;
        case 2:
            cout<<"Before sort: "<<ar<<endl;
            countSort(ar);
            cout<<"Counting sorted: "<<ar<<endl;
            break;
        case 3:
            cout<<"Vpisi stevilko"<<endl;
            cin>>x;
            if(isPrime(x)){
                cout<<x<<" je prastevilo"<<endl;
            }else
                cout<<x<<" ni prastevilo"<<endl;
            break;
        default:
            cout<<"Invalid input"<<endl;
            break;
    }
}